User Registration API

Description

This is a Spring Boot API that allows users to register and retrieve user details. A user is defined by the following attributes:

    User name
    Birthdate
    Country of residence

    Optional attributes: phone number, gender

Only adult French residents are allowed to create an account. The application includes validation and proper error handling.

Requirements

    Java 17
    Maven
    Git
    cURL or a tool like Postman for testing API endpoints

Setup Instructions

1- Clone the Repository:

    git clone https://gitlab.com/laklam/userapi.git

2- build the application :

    mvn clean install

3- run the application :

    mvn spring-boot:run

The application will start and be accessible at http://localhost:8080

Database Configuration

This application uses the embedded H2 database for simplicity. The H2 database is configured in the application.properties in the src/main/resources directory.

the dataBase is accessible at http://localhost:8080/h2-console 
with the username "sa" without a password 

Using the API

You can use cURL, Postman, or any other HTTP client to interact with the API. Below are the details of the available endpoints.

1- Register a User

    Endpoint: /api/users/register
    Method: POST
    Description: Registers a new user.
    Request Body :
    {
    "userName" : "thierry",
    "birthDate" : "1996-02-05",
    "countryName" : "France",
    "gender" : "M"
    }
    Success Response: 201 Created
    400 Bad Request if user is under 18 or not a French resident or 
    if user with the same name, birthdate, and country already exists.
   
2- Get Users by Name
    
    Endpoint: /api/users/{name}
    Method: GET
    Description: Retrieves users with the given name.
    Response: 200 OK
    Error Response: 404 Not Found if no users are found.
3- Update a User

    Endpoint: /api/users/{userName/{birthDate}
    Method: PUT
    Description: Updates the details of an existing user.
    Request Body :
    {
    "userName" : "changed name",
    "birthDate" : "1996-02-05",
    "countryName" : "France",
    "gender" : "F"
    }
    Success Response: 200 OK
    Error Response: 
    400 Bad Request if input validation fails.
    404 Not Found if no users are found

4- Delete a User
    
    Endpoint: /api/users/{userName/{birthDate}
    Method: DELETE
    Description: Deletes an existing user.
    Success Response: 204 No Content
    Error Response: 404 Not Found if user does not exist.
    
Request samples :
you'll find a request collection in postman_collection.json