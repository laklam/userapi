package com.user.userAPI;


import com.user.userAPI.entity.UserApi;
import com.user.userAPI.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class UserIntegrationTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void registerSuccessful() throws Exception {
        String user = """
                {
                    "userName" : "thierry henry",
                    "birthDate" : "1996-02-05",
                    "countryName" : "France",
                    "gender" : "M"\s
                }
                """;
        mockMvc.perform(post("/api/users/register")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(user))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.userName").value("thierry henry"))
                .andExpect(jsonPath("$.birthDate").value("1996-02-05"))
                .andExpect(jsonPath("$.countryName").value("France"))
                .andExpect(jsonPath("$.gender").value("M"));
    }

    @Test
    void registerExistingUser() throws Exception {
        String user = """
                {
                    "userName" : "thierry",
                    "birthDate" : "1996-02-05",
                    "countryName" : "France",
                    "gender" : "M"\s
                }
                """;
        UserApi existingUser = new UserApi();
        existingUser.setUserName("thierry");
        existingUser.setBirthDate(LocalDate.of(1996, 2,5));
        existingUser.setCountryName("France");
        userRepository.save(existingUser);

        mockMvc.perform(post("/api/users/register")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(user))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("User with the same name, birthdate, and country already exists."));
    }

    @Test
    void registerNonFrenchUser() throws Exception {
        String user = """
                {
                    "userName" : "Massimiliano",
                    "birthDate" : "1996-02-05",
                    "countryName" : "Italie",
                    "gender" : "M"\s
                }
                """;
        mockMvc.perform(post("/api/users/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(user))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("Only French residents are allowed to create an account!"));
    }

    @Test
    void registerNonAdultUser() throws Exception{
        String user = """
                {
                    "userName" : "max",
                    "birthDate" : "2010-02-05",
                    "countryName" : "France",
                    "gender" : "M"\s
                }
                """;
        mockMvc.perform(post("/api/users/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(user))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("User must be an adult."));
    }

    @Test
    void getExistingUserByName() throws Exception{
        UserApi existingUser = new UserApi();
        existingUser.setUserName("Antoine");
        existingUser.setBirthDate(LocalDate.of(1996, 2,5));
        existingUser.setCountryName("France");
        existingUser.setGender(Optional.of("M"));
        userRepository.save(existingUser);

        mockMvc.perform(get("/api/users/Antoine")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].userName").value("Antoine"))
                .andExpect(jsonPath("$[0].birthDate").value("1996-02-05"))
                .andExpect(jsonPath("$[0].countryName").value("France"))
                .andExpect(jsonPath("$[0].gender").value("M"));
    }

    // case where different users with the same name
    @Test
    void getExistingUsersByName() throws Exception{
        UserApi existingUser = new UserApi();
        existingUser.setUserName("joe");
        existingUser.setBirthDate(LocalDate.of(1996, 2,5));
        existingUser.setCountryName("France");
        existingUser.setGender(Optional.of("M"));

        UserApi existingUser2 = new UserApi();
        existingUser2.setUserName("joe");
        existingUser2.setBirthDate(LocalDate.of(2000, 2,5));
        existingUser2.setCountryName("France");
        existingUser2.setGender(Optional.of("M"));
        userRepository.save(existingUser);
        userRepository.save(existingUser2);

        mockMvc.perform(get("/api/users/joe")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].userName").value("joe"))
                .andExpect(jsonPath("$[0].birthDate").value("1996-02-05"))
                .andExpect(jsonPath("$[0].countryName").value("France"))
                .andExpect(jsonPath("$[0].gender").value("M"))
                .andExpect(jsonPath("$[1].userName").value("joe"))
                .andExpect(jsonPath("$[1].birthDate").value("2000-02-05"))
                .andExpect(jsonPath("$[1].countryName").value("France"))
                .andExpect(jsonPath("$[1].gender").value("M"));
    }

    @Test
    void getNoExistingUserByName() throws Exception{
        mockMvc.perform(get("/api/users/Nobody")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().string("No user found with the given name."));
    }

    @Test
    void updateExistingUser() throws Exception{
        UserApi existingUser = new UserApi();
        existingUser.setUserName("isac newten");
        existingUser.setBirthDate(LocalDate.of(1996, 2,5));
        existingUser.setCountryName("France");
        existingUser.setGender(Optional.of("M"));
        userRepository.save(existingUser);

        String updatedUser = """
                {
                    "userName" : "changed name",
                    "birthDate" : "1996-02-05",
                    "countryName" : "France",
                    "gender" : "F"\s
                }
                """;

        mockMvc.perform(put("/api/users/isac newten/1996-2-5")
                .contentType(MediaType.APPLICATION_JSON)
                .content(updatedUser))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.userName").value("changed name"))
                .andExpect(jsonPath("$.gender").value("F"));
    }

    @Test
    void updateNoExistingUser() throws Exception{
        String updatedUser = """
                {
                    "userName" : "changed name",
                    "birthDate" : "1996-02-05",
                    "countryName" : "France",
                    "gender" : "F"\s
                }
                """;
        mockMvc.perform(put("/api/users/noBody/2000-5-5")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedUser))
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteExistingUser() throws Exception{
        UserApi existingUser = new UserApi();
        existingUser.setUserName("Riyad Mahrez");
        existingUser.setBirthDate(LocalDate.of(1996, 2,5));
        existingUser.setCountryName("France");
        existingUser.setGender(Optional.of("M"));
        userRepository.save(existingUser);

        mockMvc.perform(delete("/api/users/Riyad Mahrez/1996-2-5")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    void deleteNoExistingUser() throws Exception{
        mockMvc.perform(delete("/api/users/noBody/1885-5-5")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}
