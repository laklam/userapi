package com.user.userAPI;

import com.user.userAPI.entity.UserApi;
import com.user.userAPI.repository.UserRepository;
import com.user.userAPI.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void registerSuccessfulUser(){
        UserApi user = new UserApi();
        user.setUserName("Kilyan boro");
        user.setBirthDate(LocalDate.of(2000,5,6));
        user.setCountryName("France");
        user.setGender(Optional.of("M"));
        user.setPhoneNumber(Optional.of("+33759632565"));

        when(userRepository.findByUserNameAndBirthDate(("Kilyan boro"), LocalDate.of(2000,5,6))).thenReturn(Optional.empty());
        when(userRepository.save(user)).thenReturn(user);

        UserApi registeredUser = userService.registerUser(user);

        assertNotNull(registeredUser);
        verify(userRepository,times(1)).save(user);
    }

    @Test
    void registerNonFrenchResident(){
        UserApi user = new UserApi();
        user.setUserName("Kilyan boro");
        user.setBirthDate(LocalDate.of(2000,5,6));
        user.setCountryName("Italie");

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            userService.registerUser(user);
        });

        assertEquals("Only French residents are allowed to create an account!", exception.getMessage());
    }

    @Test
    void registerNonAdultUser(){
        UserApi user = new UserApi();
        user.setUserName("Kilyan boro");
        user.setBirthDate(LocalDate.of(2015, 5,6));
        user.setCountryName("France");

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            userService.registerUser(user);
        });

        assertEquals("User must be an adult.", exception.getMessage());
    }

    @Test
    void registerExistingUser(){
        UserApi user = new UserApi();
        user.setUserName("Kilyan boro");
        user.setBirthDate(LocalDate.of(2000, 5,6));
        user.setCountryName("France");

        when(userRepository.findByUserNameAndBirthDate("Kilyan boro",LocalDate.of(2000, 5,6))).thenReturn(Optional.of(user));
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            userService.registerUser(user);
        });

        assertEquals("User with the same name, birthdate, and country already exists.", exception.getMessage());
    }

    @Test
    void getExistingUserByName(){
        UserApi user = new UserApi();
        user.setUserName("Kilyan boro");
        user.setBirthDate(LocalDate.of(2000, 5,6));
        user.setCountryName("France");

        when(userRepository.findByUserName("Kilyan boro")).thenReturn(List.of(user));
        List<UserApi> foundUsers = userService.getUserByName("Kilyan boro");

        assertNotNull(foundUsers);
        assertEquals("Kilyan boro", foundUsers.get(0).getUserName());
        verify(userRepository, times(1)).findByUserName("Kilyan boro");
    }

    // case where different users with the same name
    @Test
    void getExistingUsersByName(){
        UserApi user1 = new UserApi();
        user1.setUserName("Kilyan boro");
        user1.setBirthDate(LocalDate.of(2000, 5,6));
        user1.setCountryName("France");

        UserApi user2 = new UserApi();
        user2.setUserName("Kilyan boro");
        user2.setBirthDate(LocalDate.of(1999, 5,6));
        user2.setCountryName("France");

        when(userRepository.findByUserName("Kilyan boro")).thenReturn(List.of(user1,user2));
        List<UserApi> foundUsers = userService.getUserByName("Kilyan boro");

        assertNotNull(foundUsers);
        assertEquals(2, foundUsers.size());
        verify(userRepository, times(1)).findByUserName("Kilyan boro");
    }

    @Test
    void getNoExistingUserByName(){
         when(userRepository.findByUserName("Kilyan boro")).thenReturn(List.of());
         IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
             userService.getUserByName("Kilyan boro");
         });

         assertEquals("No user found with the given name.",exception.getMessage());
     }

    @Test
    void updateExistingUser(){
        UserApi user = new UserApi();
        user.setUserName("din ikea");
        user.setBirthDate(LocalDate.of(2000, 5,6));
        user.setCountryName("France");

        UserApi updatedUser = new UserApi();
        updatedUser.setUserName("changed name");
        updatedUser.setBirthDate(LocalDate.of(2000, 5,6));
        updatedUser.setCountryName("France");
        updatedUser.setGender(Optional.of("M"));

        when(userRepository.findByUserNameAndBirthDate("din ikea",LocalDate.of(2000,5,6))).thenReturn(Optional.of(user));
        when(userRepository.save(user)).thenReturn(user);

        UserApi returnedUser = userService.updateUser("din ikea", LocalDate.of(2000,5,6),updatedUser);
        assertEquals("changed name", returnedUser.getUserName());
        assertEquals(Optional.of("M"), returnedUser.getGender());

        verify(userRepository, times(1)).findByUserNameAndBirthDate("din ikea", LocalDate.of(2000,5,6));
        verify(userRepository, times(1)).save(user);
    }

    @Test
    void updateNoExistingUser(){
        UserApi updatedUser = new UserApi();
        updatedUser.setUserName("changed name");
        updatedUser.setBirthDate(LocalDate.of(2000, 5,6));
        updatedUser.setCountryName("France");
        updatedUser.setGender(Optional.of("M"));

        when(userRepository.findByUserNameAndBirthDate("changed name",LocalDate.of(2000,5,6))).thenReturn(Optional.empty());

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            userService.updateUser("changed name",LocalDate.of(2000,5,6),updatedUser);
        });

        assertEquals("No user found with the given name and birthDate.", exception.getMessage());
        verify(userRepository, times(1)).findByUserNameAndBirthDate("changed name",LocalDate.of(2000,5,6));
        verify(userRepository, never()).save(any(UserApi.class));
    }

    @Test
    void deleteExistingUser(){
         UserApi user = new UserApi();
         user.setUserName("din ikea");
         user.setBirthDate(LocalDate.of(2000, 5,6));
         user.setCountryName("France");

        when(userRepository.findByUserNameAndBirthDate("din ikea",LocalDate.of(2000,5,6))).thenReturn(Optional.of(user));
        doNothing().when(userRepository).deleteById(5L);

        userService.deleteUser("din ikea",LocalDate.of(2000,5,6));

        verify(userRepository, times(1)).findByUserNameAndBirthDate("din ikea",LocalDate.of(2000,5,6));
        verify(userRepository, times(1)).deleteById(user.getId());
     }

     @Test
    void deleteNoExistingUser(){
         when(userRepository.findByUserNameAndBirthDate("is ikea",LocalDate.of(2000,5,6))).thenReturn(Optional.empty());

         IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
             userService.deleteUser("is ikea",LocalDate.of(2000,5,6));
         });

         assertEquals("No user found with the given name and birthDate.", exception.getMessage());
         verify(userRepository, times(1)).findByUserNameAndBirthDate("is ikea",LocalDate.of(2000,5,6));
         verify(userRepository, never()).deleteById(1L);
     }
}
