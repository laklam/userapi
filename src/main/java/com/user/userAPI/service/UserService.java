package com.user.userAPI.service;

import com.user.userAPI.entity.UserApi;
import com.user.userAPI.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public UserApi registerUser(UserApi userApi){
        validateUser(userApi);
        return userRepository.save(userApi);
    }

    public UserApi updateUser(String userName,LocalDate birthDate,UserApi userApi){
        Optional<UserApi> foundUser = userRepository.findByUserNameAndBirthDate(userName,birthDate);
        if(foundUser.isEmpty()){
            throw new IllegalArgumentException("No user found with the given name and birthDate.");
        }

        UserApi existingUser = foundUser.get();
        existingUser.setUserName(userApi.getUserName());
        existingUser.setBirthDate(userApi.getBirthDate());
        existingUser.setCountryName(userApi.getCountryName());
        existingUser.setPhoneNumber(userApi.getPhoneNumber());
        existingUser.setGender(userApi.getGender());

        return userRepository.save(existingUser);
    }

    public List<UserApi> getUserByName(String name){
        List<UserApi>  users = userRepository.findByUserName(name);
        if (users.isEmpty()) {
            throw new IllegalArgumentException("No user found with the given name.");
        } else {
            return users;
        }
    }

    public void deleteUser(String userName, LocalDate birthDate){
        Optional<UserApi> foundUser = userRepository.findByUserNameAndBirthDate(userName,birthDate);
        if(foundUser.isEmpty()){
            throw new IllegalArgumentException("No user found with the given name and birthDate.");
        } else {
            userRepository.deleteById(foundUser.get().getId());
        }
    }

    private void validateUser(UserApi userApi){
        Optional<UserApi> existingUser = userRepository.findByUserNameAndBirthDate(userApi.getUserName(),userApi.getBirthDate());
        if (existingUser.isPresent()) {
            throw new IllegalArgumentException("User with the same name, birthdate, and country already exists.");
        }
        if (!isAdult(userApi.getBirthDate())){
            throw new IllegalArgumentException("User must be an adult.");
        }
        if (!"France".equalsIgnoreCase(userApi.getCountryName())){
            throw new IllegalArgumentException("Only French residents are allowed to create an account!");
        }
    }


    private boolean isAdult(LocalDate birthDate){
    return Period.between(birthDate, LocalDate.now()).getYears() >= 18;
    }
}
