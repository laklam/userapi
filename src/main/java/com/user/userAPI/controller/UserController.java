package com.user.userAPI.controller;


import com.user.userAPI.entity.UserApi;
import com.user.userAPI.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping(value = "/register" )
    public ResponseEntity<?> registerUser(@RequestBody UserApi userApi){
        try {
            UserApi registredUserApi = userService.registerUser(userApi);
            return ResponseEntity.status(HttpStatus.CREATED).body(registredUserApi);
        } catch (IllegalArgumentException e){
           return  ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping(value="/{name}")
    public ResponseEntity<?> GetUser(@PathVariable String name) {
        try {
            List<UserApi> users = userService.getUserByName(name);
            return ResponseEntity.ok(users);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @PutMapping(value = "/{userName}/{birthDate}")
    public ResponseEntity<?> updateUser(@PathVariable String userName,
                                        @PathVariable @DateTimeFormat(pattern = "yyyy-M-d") LocalDate birthDate,
                                        @RequestBody UserApi userApi){
        try {
            UserApi updatedUser = userService.updateUser(userName,birthDate,userApi);
            return ResponseEntity.ok(updatedUser);
        } catch (IllegalArgumentException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @DeleteMapping(value = "/{userName}/{birthDate}")
    public ResponseEntity<?> deleteUser(@PathVariable String userName,
                                        @PathVariable @DateTimeFormat(pattern = "yyyy-M-d") LocalDate birthDate){
        try {
            userService.deleteUser(userName,birthDate);
            return ResponseEntity.noContent().build();
        } catch (IllegalArgumentException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }
}
