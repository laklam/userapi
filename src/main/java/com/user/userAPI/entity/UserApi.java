package com.user.userAPI.entity;


import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.time.LocalDate;
import java.util.Objects;
import java.util.Optional;

@Entity
@Data
public class UserApi {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String userName;
    private LocalDate birthDate;
    private String countryName;
    @JdbcTypeCode(SqlTypes.JSON)
    private Optional<String> phoneNumber;
    @JdbcTypeCode(SqlTypes.JSON)
    private Optional<String> gender;

    public UserApi() {
    }

    public UserApi(Long id, String userName, LocalDate birthDate, String countryName) {
        this.id = id;
        this.userName = userName;
        this.birthDate = birthDate;
        this.countryName = countryName;
    }

    public UserApi(Long id, String userName, LocalDate birthDate, String countryName, Optional<String> phoneNumber, Optional<String> gender) {
        this.id = id;
        this.userName = userName;
        this.birthDate = birthDate;
        this.countryName = countryName;
        this.phoneNumber = phoneNumber;
        this.gender = gender;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public Optional<String> getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Optional<String> phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Optional<String> getGender() {
        return gender;
    }

    public void setGender(Optional<String> gender) {
        this.gender = gender;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserApi userApi)) return false;
        return Objects.equals(id, userApi.id) && Objects.equals(userName, userApi.userName) && Objects.equals(birthDate, userApi.birthDate) && Objects.equals(countryName, userApi.countryName) && Objects.equals(phoneNumber, userApi.phoneNumber) && Objects.equals(gender, userApi.gender);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userName, birthDate, countryName, phoneNumber, gender);
    }

    @Override
    public String toString() {
        return "UserApi{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", birthDate=" + birthDate +
                ", countryName='" + countryName + '\'' +
                ", phoneNumber=" + phoneNumber +
                ", gender=" + gender +
                '}';
    }
}
