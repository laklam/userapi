package com.user.userAPI.repository;

import com.user.userAPI.entity.UserApi;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<UserApi, Long> {
    //search a user with it ID
    @Override
    Optional<UserApi> findById(Long id);
    //search all users with a given name
    List<UserApi> findByUserName(String userName);
    //search a user with it name birthDate and country name
    Optional<UserApi> findByUserNameAndBirthDate(String userName, LocalDate birthDate);
    // delete a user with it ID
    void deleteById(Long id);
}
